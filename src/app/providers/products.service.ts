import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from './product.interface';

@Injectable()
export class ProductsService {
  products: Product[] = [];

  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    return this.http.get('./assets/data/products.json').pipe(
      map(res => {
        this.products = res  as Product[];
        return this.products;
      }));
  }
}
