import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { CartService } from './providers/cart.service';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { DetailComponent } from './detail/detail.component';
import { NosotrosPageComponent } from './nosotros-page/nosotros-page.component';
import { ProductsService } from './providers/products.service';
import { ProductsResolver } from './providers/products.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    resolve: { products: ProductsResolver }
  },
  { path: 'contact', component: ContactPageComponent },
  { path: 'detail/:id', component: DetailComponent },
  { path: 'about', component: NosotrosPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DetailComponent,
    NavBarComponent,
    FooterComponent,
    FaqPageComponent,
    ContactPageComponent,
    NosotrosPageComponent,
  ],
  imports: [BrowserModule, HttpClientModule, RouterModule.forRoot(routes)],
  providers: [CartService, ProductsService, ProductsResolver],
  bootstrap: [AppComponent]
})
export class AppModule {}
