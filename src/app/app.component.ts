import { Component, AfterViewInit } from '@angular/core';
import { CartService } from './providers/cart.service';
import { ProductsService } from './providers/products.service';

declare var $, jQuery;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'app';
  constructor(private products: ProductsService) {
    this.products.getProducts().subscribe(res => {
      console.log('productos que llegaron aqui', res);
    });
  }

  ngAfterViewInit() {
   // $.MultiLanguage('/assets/js/language.json');
  }
}
