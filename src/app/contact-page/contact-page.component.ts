import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $: any;
declare var google: any;

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit, AfterViewInit {
  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    $.MultiLanguage('/assets/js/language.json');
    function initialize() {
      const mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(49.1678136, 16.5671893),
        scrollwheel: false
      };
      const map = new google.maps.Map(
        document.getElementById('map'),
        mapOptions
      );

      const myLatLng = new google.maps.LatLng(49.1681989, 16.5650808);
      const marker = new google.maps.Marker({
        position: myLatLng,
        map: map
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
  }
}
