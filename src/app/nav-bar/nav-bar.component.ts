import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CartService } from '../providers/cart.service';
import { Product } from '../providers/product.interface';
import Swal from 'sweetalert2';

declare var $, jQuery;
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, AfterViewInit {
  visibleCart = false;
  constructor(public cart: CartService) {}

  get cartItemsCount() {
    return this.cart.items.length;
  }

  get totalCart() {
    return this.cart.getTotal();
  }
  ngOnInit() {}

  removeToCart(product: Product) {
    this.cart.removeItem(product);
  }

  clearToCart() {
    this.cart.removeAll();
  }

  sendQuotation(cartItems) {
    if (cartItems.length > 0) {
      Swal({
        title: 'Para terminar de cotizar solo debe colocar su correo',
        input: 'email',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Enviar',
        showLoaderOnConfirm: true,
        preConfirm: login => {
          return fetch(`//api.github.com/users/${login}`)
            .then(response => {
              if (!response.ok) {
                throw new Error(response.statusText);
              }
              return response.json();
            })
            .catch(error => {
              Swal.showValidationMessage(`Request failed: ${error}`);
            });
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(result => {
        if (result.value) {
          Swal({
            title: `${result.value.login}'s avatar`,
            imageUrl: result.value.avatar_url
          });
        }
      });
    } else {
      Swal({
        title: 'Error!',
        text: 'No hay lementos en el listado',
        type: 'error',
        confirmButtonText: 'ok'
      });
    }
  }

  ngAfterViewInit() {
    $(document).ready(function() {
      $.MultiLanguage('/assets/js/language.json');
    });
  }

  showCart() {
    this.visibleCart = true;
  }

  closeCart() {
    this.visibleCart = false;
  }

  nada($event: Event) {
    $event.preventDefault();
  }

  getUrlPicture(img) {
    return './assets/img/products/' + (img || 'blank.png');
  }
}
