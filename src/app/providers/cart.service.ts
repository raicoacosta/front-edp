import { Injectable } from '@angular/core';
import { Product } from './product.interface';
import * as _ from 'lodash';

@Injectable()
export class CartService {
  items: Product[] = [];
  total;
  constructor() {}
  getTotal() {
   return  _.sumBy(this.items, o => {
      return (o.quantity || 0) * o.price;
    });
  }

  addItemToCart(item: Product, qty: number) {
    let product = _.find(this.items, { id: item.id });
    if (!product) {
      product = item;
      product.quantity = parseInt(<any>qty, 10) || 1;
      this.items.push(item);
    } else {
      product.quantity = (product.quantity || 0) + (parseInt(<any>qty, 10) || 1);
    }
  }

  removeItem(item: Product) {
    _.remove(this.items, { id: item.id });
  }

  removeAll() {
    _.remove(this.items);
  }
}
