import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CartService } from '../providers/cart.service';
import { ProductsService } from '../providers/products.service';
import { Product } from '../providers/product.interface';
import * as _ from 'lodash';

declare var $, jQuery;
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit, AfterViewInit {
  pintura_especial_products: Product[] = [];
  pintura_arquitectonicas_products: Product[] = [];
  tools_products: Product[] = [];
  constructor(private cart: CartService, private products: ProductsService) {
    this.pintura_especial_products = _.filter(this.products.products, o => {
      return o.category === 'Pintura' && o.subcategory === 'Especial';
    }) as Product[];
    this.pintura_arquitectonicas_products = _.filter(this.products.products, o => {
      return o.category === 'Pintura' && o.subcategory === 'Arquitectónicas';
    }) as Product[];
    this.tools_products = _.filter(this.products.products, o => {
      return o.category === 'Herramientas y Accesorios';
    }) as Product[];
  }

  ngOnInit() {}

  getUrlPicture(img) {
    return './assets/img/products/' + (img || 'blank.png');
  }

  addToCart(product: Product, qty: number) {
    console.log('producto', product, qty);
    this.cart.addItemToCart(product, qty);
  }
  ngAfterViewInit() {
    $.MultiLanguage('/assets/js/language.json');
    $('#get-inspired').owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 300,
      paginationSpeed: 400,
      autoPlay: true,
      stopOnHover: true,
      singleItem: true,
      afterInit: ''
    });

    $('#main-slider').owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 300,
      paginationSpeed: 400,
      autoPlay: true,
      stopOnHover: true,
      singleItem: true,
      afterInit: ''
    });

    $('.product-slider').owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 300,
      paginationSpeed: 400,
      afterInit: () => {
        $('.product-slider .item').css('visibility', 'visible');
      }
    });
  }
}
