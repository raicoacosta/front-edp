import { Component, OnInit, AfterViewInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  } 
  ngAfterViewInit() {
    $.MultiLanguage('/assets/js/language.json');
  }
}
